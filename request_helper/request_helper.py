"""Sending HTTP requests with timeouts and retries."""
import logging

import requests
from requests.adapters import HTTPAdapter
from requests.exceptions import (
    ChunkedEncodingError,
    ConnectionError,
    RetryError,
)
from requests.packages.urllib3.util.retry import Retry
from textwrap import shorten


DEFAULT_RETRIES_NUMBER = 3
DEFAULT_TIMEOUT = 15  # sec.
RETRY_EXCEPTIONS = (ConnectionError, ChunkedEncodingError)
MAX_RESPONSE_TEXT_LENGTH_FOR_LOG = 50


class CallbackRetry(Retry):
    """HTTP requests retries with callback in order to log the attempts."""

    def __init__(self, *args, **kwargs):
        self._callback = kwargs.pop('callback', None)
        self._retry_processor = kwargs.pop('retry_processor', None)
        super(CallbackRetry, self).__init__(*args, **kwargs)

    def new(self, **kw):
        """Set callback for use at every attempt."""
        kw['callback'] = self._callback
        kw['retry_processor'] = self._retry_processor
        return super(CallbackRetry, self).new(**kw)

    def increment(self, method, url, *args, **kwargs):
        """Call callback method at every attempt."""
        if self._callback:
            self._callback(
                url, *args, retry_processor=self._retry_processor, **kwargs)
        return super(
            CallbackRetry, self).increment(method, url, *args, **kwargs)


def log_retry(url, *args, **kwargs):
    """Log every (bad) attempt."""
    response = kwargs.pop('response', None)
    if not response or not response.retries.history:
        return
    log_record = f'Request to URL : {url} with parameters {kwargs} failed'
    if response:
        if callable(kwargs['retry_processor']):
            kwargs['retry_processor'](response)
        body_for_log = shorten(
            response.data.decode("utf-8", errors="ignore"),
            width=MAX_RESPONSE_TEXT_LENGTH_FOR_LOG)
        log_record += (
            f', status: {response.status}, '
            f'body: {body_for_log}, '
            f'retries made: {len(response.retries.history)}, '
            f'retries left: {response.retries.total} ')
    custom_message = kwargs.pop('custom_message', None)
    if custom_message:
        log_record += f', message: {custom_message}'
    logging.warning(log_record)


default_retry_params = dict(
    total=DEFAULT_RETRIES_NUMBER,
    backoff_factor=0.1,
    status_forcelist=[500, 502, 503, 504],
    method_whitelist=['GET', 'POST', 'DELETE', 'PATCH'],
    callback=log_retry,
)

default_retries = CallbackRetry(**default_retry_params)

default_session = requests.Session()
default_session.mount('https://', HTTPAdapter(max_retries=default_retries))
default_session.mount('http://', HTTPAdapter(max_retries=default_retries))


def is_status_valid(
        status_code, custom_valid_statuses=None, custom_invalid_statuses=None):
    """Check if the status must be considered valid in this certain case."""
    return (status_code < 400 or status_code in (custom_valid_statuses or [])
            ) and status_code not in (custom_invalid_statuses or [])


def send_request(
        http_method, url, params=None, data=None, json=None, files=None,
        headers=None, timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
        custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER,
        allow_redirects=False, stream=None, auth=None, retry_processor=None):
    """
    Send the request, retry if necessary, log the results.

    Parameters
    ----------
    http_method - the HTTP method to be used
    url - URL to request
    params - URL arguments to pass
    data -  request body
    json - request body in JSON format
    files - files to upload
    headers - HTTP headers to send along
    timeout - seconds to wait before retry
    custom_valid_statuses - list of HTTP statuses to consider valid
    custom_invalid_statuses - list of HTTP statuses to consider invalid
    max_retries - number of unsuccessful attempts to make
    allow_redirects - follow redirects on appropriate statuses
    stream - download the content immediately
    auth - basic authentication info

    Returns
    -------
    HTTP response as is
    """
    if retry_processor or (max_retries != DEFAULT_RETRIES_NUMBER):
        retry_params = default_retry_params.copy()
        retry_params.update(total=max_retries, retry_processor=retry_processor)
        retries = CallbackRetry(**retry_params)
        session = requests.Session()
        session.mount('https://', HTTPAdapter(max_retries=retries))
        session.mount('http://', HTTPAdapter(max_retries=retries))
    else:
        session = default_session
    with session:
        response = None
        for _ in range(max_retries + 1):
            try:
                response = session.request(
                    http_method, url, params=params, data=data, json=json,
                    files=files, headers=headers, timeout=timeout,
                    allow_redirects=allow_redirects, stream=stream, auth=auth)
                break
            except RETRY_EXCEPTIONS as e:
                log_retry(url, params=params, custom_message=str(e))
                response = e
            except RetryError as e:
                raise e
        else:
            logging.error(f'Request to URL {url} failed with error {response}')
            raise response
    log_method = logging.info if response and is_status_valid(
        response.status_code, custom_valid_statuses,
        custom_invalid_statuses
    ) else logging.error
    content_type = response.headers.get('Content-Type', '').lower()
    if content_type.startswith(('text', 'application/json')):
        response.encoding = 'UTF-8'
        text_for_log = shorten(
            response.text, width=MAX_RESPONSE_TEXT_LENGTH_FOR_LOG)
    else:
        text_for_log = f'Content <{content_type}> not shown'
    log_method(
        f'Request to URL : {url} with parameters {params}. '
        f'Status : {response.status_code}, text: {text_for_log}')
    return response


def request_get(
        url, params=None, headers=None, timeout=DEFAULT_TIMEOUT,
        custom_valid_statuses=None, custom_invalid_statuses=None,
        max_retries=DEFAULT_RETRIES_NUMBER, allow_redirects=False,
        stream=None, auth=None, retry_processor=None):
    """
    Call send_request with GET method.

    Parameters
    ----------
    url - URL to request
    params - URL arguments to pass
    headers - HTTP headers to send along
    timeout - seconds to wait before retry
    custom_valid_statuses - list of HTTP statuses to consider valid
    custom_invalid_statuses - list of HTTP statuses to consider invalid
    max_retries - number of unsuccessful attempts to make
    allow_redirects - follow redirects on appropriate statuses
    stream - download the content immediately
    auth - basic authentication info

    Returns
    -------
    HTTP response as is
    """
    return send_request(
        'GET', url, params=params, headers=headers, timeout=timeout,
        custom_valid_statuses=custom_valid_statuses,
        custom_invalid_statuses=custom_invalid_statuses,
        max_retries=max_retries, allow_redirects=allow_redirects,
        stream=stream, auth=auth, retry_processor=retry_processor)


def request_post(
        url, params=None, data=None, json=None, files=None, headers=None,
        timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
        custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER,
        auth=None, retry_processor=None):
    """
    Call send_request with POST method.

    Parameters
    ----------
    url - URL to request
    params - URL arguments to pass
    data -  request body
    json - request body in JSON format
    files - files to upload
    headers - HTTP headers to send along
    timeout - seconds to wait before retry
    custom_valid_statuses - list of HTTP statuses to consider valid
    custom_invalid_statuses - list of HTTP statuses to consider invalid
    max_retries - number of unsuccessful attempts to make
    auth - basic authentication info

    Returns
    -------
    HTTP response as is
    """
    return send_request(
        'POST', url, params=params, data=data, json=json, files=files,
        headers=headers, timeout=timeout,
        custom_valid_statuses=custom_valid_statuses,
        custom_invalid_statuses=custom_invalid_statuses,
        max_retries=max_retries, auth=auth, retry_processor=retry_processor)


def request_delete(
        url, params=None, data=None, json=None, headers=None,
        timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
        custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER,
        auth=None, retry_processor=None):
    """
    Call send_request with DELETE method.

    Parameters
    ----------
    url - URL to request
    params - URL arguments to pass
    data -  request body
    json - request body in JSON format
    headers - HTTP headers to send along
    timeout - seconds to wait before retry
    custom_valid_statuses - list of HTTP statuses to consider valid
    custom_invalid_statuses - list of HTTP statuses to consider invalid
    max_retries - number of unsuccessful attempts to make
    auth - basic authentication info

    Returns
    -------
    HTTP response as is
    """
    return send_request(
        'DELETE', url, params=params, data=data, json=json,
        headers=headers, timeout=timeout,
        custom_valid_statuses=custom_valid_statuses,
        custom_invalid_statuses=custom_invalid_statuses,
        max_retries=max_retries, auth=auth, retry_processor=retry_processor)


def request_patch(
        url, params=None, data=None, json=None, files=None, headers=None,
        timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
        custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER,
        auth=None, retry_processor=None):
    """
    Call send_request with PATCH method.

    Parameters
    ----------
    url - URL to request
    params - URL arguments to pass
    data -  request body
    json - request body in JSON format
    files - files to upload
    headers - HTTP headers to send along
    timeout - seconds to wait before retry
    custom_valid_statuses - list of HTTP statuses to consider valid
    custom_invalid_statuses - list of HTTP statuses to consider invalid
    max_retries - number of unsuccessful attempts to make
    auth - basic authentication info

    Returns
    -------
    HTTP response as is
    """
    return send_request(
        'PATCH', url, params=params, data=data, json=json, files=files,
        headers=headers, timeout=timeout,
        custom_valid_statuses=custom_valid_statuses,
        custom_invalid_statuses=custom_invalid_statuses,
        max_retries=max_retries, auth=auth, retry_processor=retry_processor)
