"""Setup for request-helper module."""
from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='request-helper',
    version='1.0.0',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    include_package_data=True,
    install_requires=[
        'requests==2.22.0',
    ],
    test_suite='tests',
)
