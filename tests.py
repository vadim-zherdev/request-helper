"""Unit tests for request-helper module."""
import unittest
from unittest.mock import patch
import requests
from requests.exceptions import RetryError, ConnectionError
from http.server import BaseHTTPRequestHandler, HTTPServer
import socket
from threading import Thread

from request_helper import (
    request_get, request_post, request_delete, request_patch,
    DEFAULT_RETRIES_NUMBER)
from request_helper.request_helper import MAX_RESPONSE_TEXT_LENGTH_FOR_LOG


class MockResponse():
    """Fake HTTP response for mocking in the tests."""

    def __init__(self, status_code=200, text='', json=None, headers=None):
        self.status_code = status_code
        self.text = text
        self.body = json or {}
        self.headers = headers or {}

    def json(self):
        """Return JSON-content."""
        return self.body


mock_OK_response = MockResponse(status_code=200, text='Mocked OK')
mock_not_found_response = MockResponse(
    status_code=404, text='Mocked not found')


class SendRequestsTestCase(unittest.TestCase):
    """Test case for request-helper."""

    @patch('request_helper.request_helper.send_request')
    def test_get_request(self, mock_send):
        """Testing GET requests."""
        request_get('urlurlurl', params=1, headers=2, timeout=3)
        self.assertEqual(mock_send.call_count, 1)
        args, kwargs = mock_send.call_args
        self.assertEqual(args[1], 'urlurlurl')
        self.assertEqual(kwargs['params'], 1)
        self.assertNotIn('data', kwargs)
        self.assertNotIn('json', kwargs)
        self.assertEqual(kwargs['headers'], 2)
        self.assertEqual(kwargs['timeout'], 3)
        self.assertFalse(kwargs['allow_redirects'])
        self.assertIsNone(kwargs['stream'])
        self.assertIsNone(kwargs['auth'])

    @patch('request_helper.request_helper.send_request')
    def test_post_request(self, mock_send):
        """Testing POST requests."""
        request_post(
            'urlurlurl', params=1, data=4, json=5, headers=2, timeout=3,
            files='pseudo-file', auth='some_auth_info')
        self.assertEqual(mock_send.call_count, 1)
        args, kwargs = mock_send.call_args
        self.assertEqual(args[1], 'urlurlurl')
        self.assertEqual(kwargs['params'], 1)
        self.assertEqual(kwargs['data'], 4)
        self.assertEqual(kwargs['json'], 5)
        self.assertEqual(kwargs['headers'], 2)
        self.assertEqual(kwargs['timeout'], 3)
        self.assertEqual(kwargs['files'], 'pseudo-file')
        self.assertEqual('some_auth_info', kwargs['auth'])

    @patch('request_helper.request_helper.send_request')
    def test_delete_request(self, mock_send):
        """Testing DELETE requests."""
        request_delete(
            'urlurlurl', params=1, data=4, json=5, headers=2, timeout=3)
        self.assertEqual(mock_send.call_count, 1)
        args, kwargs = mock_send.call_args
        self.assertEqual(args[1], 'urlurlurl')
        self.assertEqual(kwargs['params'], 1)
        self.assertEqual(kwargs['data'], 4)
        self.assertEqual(kwargs['json'], 5)
        self.assertEqual(kwargs['headers'], 2)
        self.assertEqual(kwargs['timeout'], 3)

    @patch('request_helper.request_helper.send_request')
    def test_patch_request(self, mock_send):
        """Testing PATCH requests."""
        request_patch(
            'urlurlurl', params=1, data=4, json=5, headers=2, timeout=3)
        self.assertEqual(mock_send.call_count, 1)
        args, kwargs = mock_send.call_args
        self.assertEqual(args[1], 'urlurlurl')
        self.assertEqual(kwargs['params'], 1)
        self.assertEqual(kwargs['data'], 4)
        self.assertEqual(kwargs['json'], 5)
        self.assertEqual(kwargs['headers'], 2)
        self.assertEqual(kwargs['timeout'], 3)

    @patch('request_helper.request_helper.default_session.request')
    def test_logging_levels(self, mock_response):
        """Logs must be added at corresponding levels."""
        mock_response.return_value = mock_OK_response
        with self.assertLogs(level='INFO') as log:
            response = request_get('urlurlurl')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(log.records), 1)
        mock_response.return_value = mock_not_found_response
        with self.assertLogs(level='ERROR') as log:
            response = request_get('urlurlurl')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(len(log.records), 1)

    @patch('request_helper.request_helper.default_session.request')
    def test_valid_invalid_statuses_list(self, mock_get):
        """List of invalid statuses must be considered."""
        mock_get.return_value = mock_not_found_response
        with self.assertLogs(level='ERROR') as log:
            request_get('urlurlurl')
        self.assertEqual(len(log.output), 1)
        with self.assertLogs(level='INFO') as log:
            request_get('urlurlurl', custom_valid_statuses=[404])
        self.assertEqual(len(log.output), 1)
        with self.assertLogs(level='ERROR') as log:
            request_get(
                'urlurlurl', custom_valid_statuses=[404],
                custom_invalid_statuses=[404])
        self.assertEqual(len(log.output), 1)
        mock_get.return_value = mock_OK_response
        with self.assertLogs(level='INFO') as log:
            request_get('urlurlurl')
        self.assertEqual(len(log.output), 1)
        with self.assertLogs(level='ERROR') as log:
            request_get('urlurlurl', custom_invalid_statuses=[200, 100500])
        self.assertEqual(len(log.output), 1)

    @patch('request_helper.request_helper.default_session.request')
    def test_long_response_trimmed(self, mock_send):
        """Test long response text trimming."""
        mock_send.return_value = MockResponse(
            text=', '.join([f'item{i}' for i in range(1_000)]),
            headers={'Content-Type': 'text'})
        with self.assertLogs(level='INFO') as logs:
            request_get('someurl')
        log_rec = logs.records[0].getMessage()
        text_pos = log_rec.find(', text: ') + 8
        text_len = len(log_rec) - text_pos
        self.assertLess(text_len, MAX_RESPONSE_TEXT_LENGTH_FOR_LOG)
        self.assertGreater(text_len, MAX_RESPONSE_TEXT_LENGTH_FOR_LOG - 10)
        self.assertEqual(' [...]', log_rec[-6:])

        mock_send.return_value = MockResponse(
            text='some short text',
            headers={'Content-Type': 'application/json'})
        with self.assertLogs(level='INFO') as logs:
            request_get('someurl')
        self.assertEqual(
            ', text: some short text', logs.records[0].getMessage()[-23:])

    @patch('request_helper.request_helper.default_session.request')
    def test_unknown_response_not_logged(self, mock_send):
        """Test response with unknown content type logging."""
        mock_send.return_value = MockResponse(
            text='some short text', headers={'Content-Type': 'strange'})
        with self.assertLogs(level='INFO') as logs:
            request_get('someurl')
        self.assertTrue(
            logs.records[0].getMessage().endswith(
                ', text: Content <strange> not shown'))


class MockServerRequestHandler(BaseHTTPRequestHandler):
    """Fake HTTP server to handle requests in the tests."""

    bad_responses_limit = 5

    def do_GET(self):
        """Return good response on the second attempt only."""
        self.send_response(
            requests.codes.server_error
            if MockServerRequestHandler.bad_responses_limit > 0
            else requests.codes.ok)
        MockServerRequestHandler.bad_responses_limit -= 1
        self.end_headers()
        return


def get_free_port():
    """Get random available port for testing purposes."""
    s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    address, port = s.getsockname()
    s.close()
    return port


class RetriesTestCase(unittest.TestCase):
    """Test case for retries while requesting."""

    @classmethod
    def setUpClass(cls):
        """Perform common setup for all tests within the test case."""
        # Configure mock server.
        cls.mock_server_port = get_free_port()
        cls.mock_server = HTTPServer(
            ('localhost', cls.mock_server_port), MockServerRequestHandler)

        # Start running mock server in a separate thread.
        # Daemon threads automatically shut down when the main process exits.
        cls.mock_server_thread = Thread(target=cls.mock_server.serve_forever)
        cls.mock_server_thread.setDaemon(True)
        cls.mock_server_thread.start()

    def retries_processor(self, response):
        """Increase the counter for custom retries processing."""
        self.retries_processor_call_count += 1

    def test_retries(self):
        """Ensure the mofule attempts to request again after bad response."""
        response = None
        with self.assertRaises(RetryError):
            with self.assertLogs(level='ERROR') as logs:
                response = request_get(
                    f'http://localhost:{self.mock_server_port}', max_retries=0)
        self.assertIsNone(response)
        with self.assertLogs(level='WARNING') as logs:
            response = request_get(
                f'http://localhost:{self.mock_server_port}', max_retries=6)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(logs.records), 3)
        self.mock_server.RequestHandlerClass.bad_responses_limit = 5
        response = None
        with self.assertRaises(RetryError):
            with self.assertLogs(level='WARNING') as logs:
                response = request_get(
                    f'http://localhost:{self.mock_server_port}', max_retries=4)
        self.assertIsNone(response)

    def test_retry_custom_processing(self):
        """Testing custom retries processing."""
        self.mock_server.RequestHandlerClass.bad_responses_limit = 5
        self.retries_processor_call_count = 0
        with self.assertRaises(RetryError):
            request_get(
                f'http://localhost:{self.mock_server_port}',
                retry_processor=self.retries_processor)
        self.assertEqual(
            DEFAULT_RETRIES_NUMBER, self.retries_processor_call_count)


class NoResponseTestCase(unittest.TestCase):
    """Testing the case when no response comes."""

    def test_no_response_processed(self):
        """The response and the logging is correct in case of no response."""
        with self.assertRaises(ConnectionError):
            with self.assertLogs(level='ERROR') as logs:
                request_get(
                    f'http://localhost:{get_free_port()}', max_retries=1)
        self.assertEqual(len(logs.records), 1)


if __name__ == '__main__':
    unittest.main()
