# Send HTTP-requests with timeouts and retries

The timeout is set in seconds by `DEFAULT_TIMEOUT`, the default is 3.

Retries number is set by `DEFAULT_RETRIES_NUMBER`, the default is 3.

Only GET, POST, DELETE, and PATCH method are retried on statuses 
500, 502, 503, 504.

Additionally, certain `requests` exceptions can be caught, and the requests
retried. Those exceptions are listed in `RETRY_EXCEPTIONS` tuple. Other
exceptions are raised as usual, including `requests.exceptions.RetryError`,
which is raised when all retries are made without acceptable response.
In case of no response (connection refused, etc.), raises `ConnectionError`.


```python
from request_helper import (
request_get, request_post, request_delete, request_patch, DEFAULT_TIMEOUT, DEFAULT_RETRIES_NUMBER)

request_get(url, params=None, headers=None, timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
    custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER,
    auth=('user', 'password'))

request_post(
    url, params=None, data=None, json=None, headers=None, timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
    custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER)

request_delete(
    url, params=None, data=None, json=None, headers=None, timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
    custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER)

request_patch(
    url, params=None, data=None, json=None, headers=None, timeout=DEFAULT_TIMEOUT, custom_valid_statuses=None,
    custom_invalid_statuses=None, max_retries=DEFAULT_RETRIES_NUMBER)
``````

## Arguments

* `url` - the only arbitrary positional parameter
* `params` - dict with url-arguments
* `headers` - dict with http headers
* `timeout` - float with the timeout in seconds
* `custom_valid_statuses` - list of HTTP-statuses that will be logged at INFO
level (i.e., acceptable statuses, expected for this certain request)
* `custom_invalid_statuses` - list of HTTP-ответов that will be logged at ERROR
level (i.e., unacceptable statuses, showing some error)
* `max_retries` - the number of attempts in case of 500, 502, 503, 504 status 
or when the timeout is expired.
* `auth` - two-tuple of the username and the password for basic authentication
* `retry_processor` - method to call on each retry. The method must accept
HTTP response as the only positional argument. Exceptions can be raised
to prevent further retrying.

##### Notes about the statuses lists:
* If neither `custom_valid_statuses` nor `custom_invalid_statuses` is specified, 
standard behaviour is implemented:: statuses up to 399 are considered normal and 
are logged and INFO level, statuses 400 and higher are considered errors and logged
and ERROR level.
* `custom_valid_statuses` list is an **addition** to the standard list of "good"
statuses. It may make sense to include 400+ statuses here, it they are normal
in your certain case.
* `custom_invalid_statuses` list is an **addition** to the standard list of "bad"
statuses. It may make sense to included statuses up to 399 here if they point
to some error in your case.
* If the same status is included into both `custom_valid_statuses` and
`custom_invalid_statuses`, it will be logged at ERROR level, i.e. `custom_invalid_statuses`
 list has higher priority. 

### arguments for GET only
* `allow_redirects` - follow redirects on 3XX statuses. False by default.
* `stream` - download the content immediately. None by default.

### agruments for POST, PATCH и DELETE only
* `data` - request body
* `json` - request body in JSON-format

### fule uploading for POST and PATCH
* `files` - dict with information about the file to be uploaded.  
The key is the parameter name.  
The value is three-tuple: 
  - file name
  - file-like object (e.g., opened by `open`)
  - file type (`application/jpeg`, etc.)
 
 Example:
 ```python
files={
    'image_to_upload': (
        'my_funny_picture.jpg',
        open('my_funny_picture.jpg', 'rb'),
        'application/jpeg',
    )
}
```
## Returning value

Returns what is returned by corresponding requests.get amd request.post  
If there were retries, so the last one. As is, i.e. [requests.response](http://docs.python-requests.org/en/master/user/quickstart/#response-content).

## Logging

Adds to the log the result of each attempt: the status and the content of the
response, at corresponding level. See description of `custom_valid_statuses` and
`custom_invalid_statuses` arguments above.

In case of retries logs at WARNING level the information about the request that was
repeated, its data, the response, the number of attempts made and attempts left.

Long response texts are trimmed when logging to `MAX_RESPONSE_TEXT_LENGTH_FOR_LOG`
symbols as descibed in [textwrap.shorten](https://docs.python.org/3.7/library/textwrap.html#textwrap.shorten).

Response text is logged only for textual response content types and for
applicaion/json response content type. For other types the name of the type
is logged instead. The response text is decoded from UTF-8 while ignoring
any errors, i.e. text that cannot be decoded logged unchanged.

## Installation
### requirements
`git+ssh://git@gitlab00.rheosrx.lan/shared-code-libraries/reqwuest-helper.git@release-X.X.X`
> Put the latest release number from the repository tags

### setup.py
```python
setup(
    # other options
    install_requires=[
        # other requirements
        'request-helper==X.X.X',
    ],
    dependency_links=[
        ('git+ssh://git@gitlab00.rheosrx.lan/shared-code-libraries/'
         'request-helper.git#egg=request-helper-X.X.X'),
    ],
)
```
> Put the latest release number from the repository tags

## Testing

```bash
python setup.py test
```
